function setCatalogueBehavior() {
	var options = {
		block: '.js-catalogue',
		links: '.js-catalogue-add',
		modal: '.js-catalogue-modal'
	}

	var $block = $(options.block);
	var $links = $block.find(options.links);
	var $modal = $block.find(options.modal);



	$links.on('click', function(e) {
		e.preventDefault();

		var $parent = $(this).parent();
		$links.parent().not($parent).removeClass('active');

		$parent.toggleClass('active');
	});

	$(document).click(function(e) {
		var $el = $(event.target);

		if ($el.closest(options.block).length) return;

		$($links.parent()).removeClass('active');

        e.stopPropagation();
	});

	$('.js-scroll a').on('click', function(e) {
		e.preventDefault();
		$(this).toggleClass('ok');
	})

	$modal.find('.js-scroll').jScrollPane();
};