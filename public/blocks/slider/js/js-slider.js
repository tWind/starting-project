(function() {

	var options = {
		stopOnHover: true,
		autoplay: 5000,
		slideSpeed : 900,
		paginationSpeed : 500,
		items : 1,
		itemsDesktop : false,
		itemsDesktopSmall : false,
		itemsTablet: false,
		itemsMobile : false
  	}

  	var $block = $(".js-slider");

	$.each($block, function() {
		var $el = $(this);
		var settings = $el.data('slider-settings');

		$el.owlCarousel($.extend({}, options, settings));
	});

 })();